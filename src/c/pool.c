#include "pool.h"

void pool_packet_deep_cpy(pool_packet_t* dest, pool_packet_t* src) {
	dest->gid = src->gid;
	dest->ttl = src->ttl;
	dest->coefs_count = src->coefs_count;
	dest->vals_count = src->vals_count;

	dest->coefs = malloc(src->coefs_count * sizeof(genkv_coef_t));
	dest->vals = malloc(src->vals_count * sizeof(uint8_t));

	memcpy(dest->coefs, src->coefs, src->coefs_count * sizeof(genkv_coef_t));
	memcpy(dest->vals, src->vals, src->vals_count * sizeof(uint8_t));
}

void pool_packet_free(pool_packet_t* pp) {
	free(pp->coefs);
	free(pp->vals);
}


void pool_init(pool_t* p, void* handle, deliver_fx_t deliver, send_fx_t send, uint8_t fanout, uint16_t max_rank, uint16_t split, uint16_t gen_hist_size, uint8_t pol, uint8_t ttl) {
	if (split == 0) split = 1;
	uint16_t rand_int;
	syscall(SYS_getrandom, &rand_int, sizeof(uint16_t), 0);
	p->current_generation = rand_int % split;

	ffinit(&(p->ctx), pol);
	dict_init(&(p->generations), 1 << 16);

	if (fanout == 0) fanout = 1;
	p->packet_buffer = malloc(sizeof(pool_packet_t) * fanout);

	if (gen_hist_size != 0) {
		p->gen_hist = malloc(sizeof(uint16_t) * gen_hist_size);
		p->gen_hist = memset(p->gen_hist, 0, sizeof(uint16_t) * gen_hist_size);
		p->gen_hist_cursor = 0;
		p->gen_last_action = 0;
	}
	p->gen_hist_size = gen_hist_size;
	p->fanout = fanout;
	p->max_rank = max_rank;
	p->split = split;
	p->deliver = deliver;
	p->send = send;
	p->handle = handle;
	p->initial_ttl = ttl;
}

genkv_t* pool_get_gen(pool_t* p, uint32_t gid) {
	char key[5] = {0};
	memcpy(key, &gid, sizeof(uint32_t));
	genkv_t* gen = dict_get(&(p->generations), key);
	return gen;
}


genkv_t* __pool_get_gen(pool_t* p, uint32_t gid) {
	char key[5] = {0};
	memcpy(key, &gid, sizeof(uint32_t));
	genkv_t* gen = dict_get(&(p->generations), key);
	if (gen != NULL) return gen;

	/* Will be freed in pool_free and dict_free */
	char* key_add = malloc(5 * sizeof(char));
	strcpy(key_add, key);
	genkv_t* gen_add = malloc(sizeof(genkv_t));
	genkv_init(gen_add, &(p->ctx), POOL_LINES_ALLOC);
	dict_add(&(p->generations), key_add, gen_add);

	return gen_add;
}

void __pool_next_gen(pool_t* p, uint32_t gid) {
	while (gid > p->current_generation) {
		p->current_generation += p->split;
	}
	
	genkv_t* gen = __pool_get_gen(p, p->current_generation);
	if (genkv_rank(gen) >= p->max_rank) {
		p->current_generation += p->split;
	}
}

void pool_recode(pool_t* p, pool_packet_t* pkt, uint32_t gid, uint8_t ttl)
{
	genkv_t* gen = __pool_get_gen(p, gid);

	pkt->gid = gid;
	pkt->ttl = ttl;
	pkt->coefs_count = gen->gen.coef_size;
	pkt->vals_count = gen->gen.val_size;
	pkt->coefs = malloc(pkt->coefs_count * sizeof(genkv_coef_t*));
	pkt->vals = malloc(pkt->vals_count * sizeof(uint8_t));

	genkv_recode(gen, pkt->coefs, pkt->vals);
} 

void __pool_disseminate(pool_t* p, genkv_t* gen, uint32_t gid, uint8_t ttl) {
	if (ttl > 0 && p->initial_ttl > 0) {
		ttl -= 1;
	}

	for (int i = 0; i < p->fanout; i++) {
		pool_packet_t* pkt = &(p->packet_buffer[i]);
		pkt->gid = gid;
		pkt->ttl = ttl;
		pkt->coefs_count = gen->gen.coef_size;
		pkt->vals_count = gen->gen.val_size;
		pkt->coefs = malloc(pkt->coefs_count * sizeof(genkv_coef_t*));
		pkt->vals = malloc(pkt->vals_count * sizeof(uint8_t));

		genkv_recode(gen, pkt->coefs, pkt->vals);
	}
	
	(*(p->send))(p->handle, p->packet_buffer, p->fanout);

	for (int i = 0; i < p->fanout; i++) {
		pool_packet_t* pkt = &(p->packet_buffer[i]);
		free(pkt->coefs);
		free(pkt->vals);
	}
}

void __pool_deliver(pool_t* p, genkv_t* gen) {
	uint16_t len;
	uint8_t** buffer = genkv_deliver(gen, &len); 

	for (uint16_t i = 0; i < len; i++) {
		(*(p->deliver))(p->handle, buffer[i], gen->gen.val_size);
	}
}

void __pool_gen_hist_update(pool_t* p, uint16_t g) {
	if (p->gen_hist_size == 0) return;

	p->gen_hist[p->gen_hist_cursor] = g;
	p->gen_hist_cursor = (p->gen_hist_cursor + 1) % p->gen_hist_size;

	uint16_t min_g = p->gen_hist[0];
	for (uint16_t i = 1; i < p->gen_hist_size; i++) {
		if (min_g > p->gen_hist[i]) min_g = p->gen_hist[i];
	}

	for (uint16_t j = p->current_generation; j >= min_g-1 && j > p->gen_last_action; j--) {
		genkv_t* max_gen = __pool_get_gen(p, j);
		if (max_gen->known_ids_count > 20) {
			if (p->max_rank == 0) p->split++;
			else p->max_rank--;
			p->gen_last_action = j;
		}
	}

	
	if (min_g > 0 && min_g - 1 > p->gen_last_action) {
		genkv_t* min_gen = __pool_get_gen(p, min_g-1);
		if (min_gen->known_ids_count < 20) {
			if (p->split == 1) p->max_rank++;
			else p->split--;
			p->gen_last_action = min_g - 1;
		}
	}
}

int pool_receive(pool_t* p, pool_packet_t* pkt) {
	genkv_t* gen = __pool_get_gen(p, pkt->gid);

	int redundant = genkv_add(gen, pkt->coefs, pkt->coefs_count, pkt->vals, pkt->vals_count);
	if (!redundant) {
		__pool_deliver(p, gen);
		if (pkt->ttl > 0) {
		  __pool_disseminate(p, gen, pkt->gid, pkt->ttl);
		}
		__pool_next_gen(p, pkt->gid);
		__pool_gen_hist_update(p, pkt->gid);
	}
	return redundant;
}

void pool_broadcast(pool_t* p, unsigned char* msg, uint16_t msg_len) {
	uint32_t msg_id;
	if (syscall(SYS_getrandom, &msg_id, sizeof(uint32_t), 0) == -1) {
		printf("ERROR. getentropy call failed\n");
	}
	pool_broadcast_msgid(p, msg_id, msg, msg_len);
}

void pool_broadcast_msgid(pool_t* p, uint32_t msg_id, unsigned char* msg, uint16_t msg_len) {

	/* Set coef */
	genkv_coef_t coef;
        coef.msg_id = msg_id;
	coef.coef_value = 1;

	/* Add to current gen */
	genkv_t* gen = __pool_get_gen(p, p->current_generation);
	genkv_add_local(gen, &coef, 1, msg, msg_len);
	
	/* Disseminate and change gen if needed */
	__pool_disseminate(p, gen, p->current_generation, MAX(1, p->initial_ttl));
	__pool_next_gen(p, p->current_generation);
	__pool_gen_hist_update(p, p->current_generation);
}

void pool_free(pool_t* p) {
	free(p->packet_buffer);
	/* @FIXME memory not elegantly freed */
	for (unsigned int i = 0; i < p->generations.entries_size; i++) {
		genkv_t* gen = p->generations.entries[i].data;
		genkv_free(gen);
	}
	dict_free(&(p->generations));
}
