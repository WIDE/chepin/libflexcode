#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <search.h>
#include "gf256.h"
#include "generation.h"
#include "generation_kv.h"

generation_t* add_gen(char* name) {
	/* Create Generation */
	generation_t* gen = malloc(sizeof(generation_t));
	
	/* Copy string content to preserve it
	 * We add +1 as strlen doesn't count the 0x00 byte
	 */
	char* nameal = malloc(( 1 + strlen(name)) * sizeof(char));
	strcpy(nameal, name);

	/* Build entries */
	ENTRY ent = { .key = nameal, .data = gen};
	hsearch(ent, ENTER);

	/* @FIXME register keys and data in a list */

	return gen;
}

generation_t* get_gen(char* name) {
	ENTRY ent = { .key = name, .data = NULL };
	ENTRY* ret = hsearch(ent, FIND);
	return ret->data;
}

void free_gen() {
	hdestroy();
	/* @FIXME free keys and generations */
}

void handle_add(FILE* read_trace_fd) {
	unsigned int coef_size, val_size;
	char gen_id[64];
	if (fscanf(read_trace_fd, "%s %u %u", gen_id, &coef_size, &val_size) != 3) {
		printf("Unable to parse gen_add parameters\n");
		exit(1);
	}
	#ifdef VERBOSE
	printf("cmd=gen_add gen=%s coef_size=%u val_size=%u\n", gen_id, coef_size, val_size);
	#endif

	unsigned char* coefs = malloc(sizeof(unsigned char) * coef_size);
	unsigned char* vals = malloc(sizeof(unsigned char) * val_size);
	if (coefs == NULL || vals == NULL) {
		printf("Failed to malloc arrays for coefs and vals in gen_add\n");
	}

	for (unsigned int i = 0; i < coef_size; i++) {
		if (fscanf(read_trace_fd, "%hhu", &coefs[i]) != 1) {
			printf("Unable to read coef from gen_add\n");
			exit(1);
		}
	}

	for (unsigned int i = 0; i < val_size; i++) {
		if (fscanf(read_trace_fd, "%hhu", &vals[i]) != 1) {
			printf("Unable to read val from gen_add\n");
			exit(1);
		}
	}

	generation_t* gen = get_gen(gen_id);
	if (gen == NULL) {
		printf("Generation %s has not been found in the hashtable\n", gen_id);
		exit(1);
	}

	gen_add(gen, coefs, coef_size, vals, val_size);
	free(coefs);
	free(vals);
}

void handle_recode(FILE* read_trace_fd) {
	unsigned int coef_size, val_size;
	char gen_id[64];
	if (fscanf(read_trace_fd, "%s %u %u", gen_id, &coef_size, &val_size) != 3) {
		printf("Unable to parse gen_recode parameters\n");
		exit(1);
	}
	#ifdef VERBOSE
	printf("cmd=gen_recode gen=%s coef_size=%u val_size=%u\n", gen_id, coef_size, val_size);
	#endif

	generation_t* gen = get_gen(gen_id);
	if (gen == NULL) {
		printf("Generation %s has not been found in the hashtable\n", gen_id);
		exit(1);
	}

	unsigned char* coefs = malloc(sizeof(unsigned char) * coef_size);
	unsigned char* vals = malloc(sizeof(unsigned char) * val_size);
	gen_recode(gen, coefs, vals);
	free(coefs);
	free(vals);
}

void handle_init(FILE* read_trace_fd, ctx_t* ctx) {
	unsigned int init_size;
	char gen_id[64];
	char ctx_id[64];
	if(fscanf(read_trace_fd, "%s %s %u", gen_id, ctx_id, &init_size) != 3) {
		printf("Unable to parse gen_init parameters\n");
		exit(1);
	}
	generation_t* gen = add_gen(gen_id);
	gen_init(gen, ctx, init_size);
	#ifdef VERBOSE
	printf("cmd=gen_init gen=%s ctx=%s init=%u\n", gen_id, ctx_id, init_size);
	#endif
}

void parse_file(FILE* read_trace_fd, ctx_t* ctx, long trace_size) {
	/* Ugly parser */
	char buffer[64];
	unsigned char npercent, opercent = 0.0f;
	while (fscanf(read_trace_fd, "%s", &buffer) == 1) {
		if (strcmp(buffer, "gen_add") == 0) handle_add(read_trace_fd);
		else if (strcmp(buffer, "gen_recode") == 0) handle_recode(read_trace_fd);
		else if (strcmp(buffer, "gen_init") == 0) handle_init(read_trace_fd, ctx);
		else {
			printf("Unknown function %s\n", buffer);
			exit(1);
		}

        	long current = ftell(read_trace_fd);
		npercent = (unsigned char)((double)current * 100.0f / (double)trace_size);
		if (npercent != opercent) {
		  printf("%d%% done\n", npercent);
		  opercent = npercent;
		}
	}
}

int main(int argc, char **argv) {
	if (argc != 2) {
		printf("Usage: %s trace\n", argv[0]);
		return 1;
	}
	FILE* read_trace_fd = fopen(argv[1], "r");
	if (read_trace_fd == NULL) {
		printf("Unable to fopen trace %s\n", argv[1]);
		return 1;
	}

	/* Get file size */
	if (fseek(read_trace_fd, 0L, SEEK_END)) {
		printf("Unable to go at the end of the file\n");
		return 1;
	}
        long trace_size = ftell(read_trace_fd);
	rewind(read_trace_fd);

	/* Init vars */
	hcreate(100000);
	ctx_t ctx;
	ffinit(&ctx, POL);

	printf("start parsing %s (%ld bytes)...\n", argv[1], trace_size);
	parse_file(read_trace_fd, &ctx, trace_size);
	printf("...done!\n");

	free_gen();

	if (fclose(read_trace_fd) == EOF) {
		printf("Unable to close file descriptor for %s\n", argv[1]);
	}

	return 0;
}
